<?php

namespace App\Importer;

use App\Fetcher\FetchResult;

/**
 * ImporterInterface interface.
 */
interface ImporterInterface
{
    /**
     * @param FetchResult $fetchResult
     */
    public function import(FetchResult $fetchResult);
}
