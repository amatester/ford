<?php

namespace App\Helper;

use Symfony\Component\Console\Output\OutputInterface;

/**
 * Helper methods.
 */
class Console
{
    /**
     * @var OutputInterface
     */
    private static $output;

    /**
     * @param OutputInterface $output
     */
    public static function setOutput(OutputInterface $output)
    {
        self::$output = $output;
    }

    /**
     * @param string $message
     * @return string
     */
    public static function trace($message)
    {
        return self::$output->writeln(
            sprintf('[%s] %s', date('Y-m-d H:i:s'), $message)
        );
    }

    /**
     * @param int $seconds
     */
    public static function sleep($seconds)
    {
        self::trace(sprintf('Sleeping %d seconds...', $seconds));
        sleep($seconds);
    }
}
