<?php

namespace App\Toyota\Repository;

use App\Toyota\Entity\ToyotaPart;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * Toyota parts repository.
 */
class ToyotaPartRepository extends ServiceEntityRepository implements IToyotaPartRepository
{
    /**
     * @param ManagerRegistry $registry
     */
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ToyotaPart::class);
    }

    /**
     * @inheritdoc
     */
    public function create(ToyotaPart $part)
    {
        $this->getEntityManager()->persist($part);
        $this->getEntityManager()->flush();

        return $part;
    }

    /**
     * @inheritdoc
     */
    public function findByNumber($number)
    {
        return $this->createQueryBuilder('part')
            ->where('part.number = :number')
            ->setParameter('number', (string)$number)
            ->getQuery()
            ->getOneOrNullResult();
    }
}
