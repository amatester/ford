<?php

namespace App\Nissan\Repository;

use App\Nissan\Entity\Part;

/**
 * Repository for lexus parts.
 */
interface IPartRepository
{
    /**
     * @param Part $part
     * @return Part
     */
    public function save(Part $part);

    /**
     * @param string $number
     * @return Part
     */
    public function findByNumber($number);
}
