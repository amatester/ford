<?php

namespace DoctrineMigrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;
use Doctrine\DBAL\Types\Type;

/**
 * Change capacity of `unit` in part tables.
 */
class Version20180428045327 extends AbstractMigration
{
    /**
     * @var string
     */
    private $toyotaParts = 'toyota_part';
    /**
     * @var string
     */
    private $lexusParts = 'lexus_part';
    /**
     * @var string
     */
    private $unit = 'unit';

    /**
     * @inheritdoc
     */
    public function up(Schema $schema)
    {
        $schema->getTable($this->toyotaParts)
            ->changeColumn($this->unit, [
                'type' => Type::getType(Type::TEXT)
            ]);
        $schema->getTable($this->lexusParts)
            ->changeColumn($this->unit, [
                'type' => Type::getType(Type::TEXT)
            ]);
    }

    /**
     * @inheritdoc
     */
    public function down(Schema $schema)
    {
        $schema->getTable($this->toyotaParts)
            ->changeColumn($this->unit, [
                'type' => Type::getType(Type::STRING)
            ]);
        $schema->getTable($this->lexusParts)
            ->changeColumn($this->unit, [
                'type' => Type::getType(Type::STRING)
            ]);
    }
}
