<?php

namespace App\Fetcher;

use App\Helper\Console;
use App\Proxy\IProxyClient;
use App\Proxy\Proxy;
use App\Queue\Queue;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\Promise\PromiseInterface;
use Psr\Http\Message\ResponseInterface;

/**
 * IFetcher implementation.
 */
class Fetcher implements IFetcher
{
    /**
     * @var IProxyClient
     */
    private $proxyClient;
    /**
     * @var Client
     */
    private $httpClient;

    /**
     * @param IProxyClient $proxyClient
     */
    public function __construct(IProxyClient $proxyClient)
    {
        $this->proxyClient = $proxyClient;
        $this->httpClient = new Client();
    }

    /**
     * @inheritdoc
     */
    public function fetch($queue, $donorName)
    {
        $proxies = $this->proxyClient->getProxies(count($queue));
        if (! $proxies) {
            Console::trace('No proxies');
        }

        $key = 0;
        $fetchResults = [];
        $promises = [];
        foreach ($queue as $queueItem) {
            $proxy = $proxies[$key] ?? null;
            $key++;

            if (! $proxy) {
                $fetchResults[] = (new FetchResult())
                    ->setResult(FetchResult::RESULT_FAIL)
                    ->setQueueItem($queueItem);
                continue;
            }

            $promises[$queueItem->getId()] = $this->createAsyncRequest($queueItem, $proxy, $fetchResults, $donorName);
        }

        if (! $promises) {
            return [];
        }

        \GuzzleHttp\Promise\unwrap($promises);
        \GuzzleHttp\Promise\settle($promises)->wait();

        return $fetchResults;
    }

    /**
     * @param Queue $queueItem
     * @param Proxy $proxy
     * @param array $fetchResults
     * @param string $donorName
     * @return PromiseInterface
     */
    private function createAsyncRequest(Queue $queueItem, Proxy $proxy, &$fetchResults, $donorName)
    {
        Console::trace(sprintf(
            'REQUEST (#%d): %s [%s]',
            $queueItem->getId(),
            $queueItem->getUrl(),
            $proxy->getProxyString()
        ));

        $url = $queueItem->getUrl();
        $successMarker = DonorMarkers::getMarker($donorName);

        return $this->httpClient
            ->getAsync($url, [
                'proxy' => $proxy->getRequestHost(),
                'timeout' => $proxy->getTimeout(),
                'connect_timeout' => $proxy->getTimeout(),
                'allow_redirects' => [
                    'max' => 2,
                    'strict' => true,
                    'referer' => true,
                    'protocols' => ['https']
                ]
            ])
            ->then(
                function (ResponseInterface $response) use ($queueItem, $proxy, &$fetchResults, $successMarker) {
                    $content = (string)$response->getBody();

                    $result = (new FetchResult())
                        ->setQueueItem($queueItem)
                        ->setResult(FetchResult::RESULT_SUCCESS)
                        ->setContent($content);

                    if (! strpos($content, $successMarker)) {
                        $result->setResult(FetchResult::RESULT_FAIL);
                        $this->markBadProxy($proxy);
                    }

                    $fetchResults[] = $result;
                },
                function (RequestException $exception) use ($queueItem, $proxy, &$fetchResults) {
                    $fetchResults[] = (new FetchResult())
                        ->setQueueItem($queueItem)
                        ->setResult(FetchResult::RESULT_FAIL);

                    $this->markBadProxy($proxy);
                }
            );
    }

    /**
     * @param Proxy $proxy
     */
    private function markBadProxy(Proxy $proxy)
    {
        Console::trace(sprintf('Bad proxy: %s', $proxy->getProxyString()));

//        $this->proxyClient->sendBadProxy($proxy);
    }
}
