<?php

namespace App\Nissan\Importer;

use App\Fetcher\FetchResult;
use App\Helper\Console;
use App\Importer\ImporterInterface;
use App\Nissan\Analyzer\PartPageAnalyzer;
use App\Nissan\Repository\IPartRepository;

/**
 * Nissan part importer.
 */
class PartImporter implements ImporterInterface
{
    /**
     * @var PartPageAnalyzer
     */
    private $analyzer;
    /**
     * @var IPartRepository
     */
    private $partRepository;

    /**
     * @param PartPageAnalyzer $analyzer
     * @param IPartRepository $partRepository
     */
    public function __construct(PartPageAnalyzer $analyzer, IPartRepository $partRepository)
    {
        $this->analyzer = $analyzer;
        $this->partRepository = $partRepository;
    }

    /**
     * @param FetchResult $fetchResult
     */
    public function import(FetchResult $fetchResult)
    {
        $queueItemId = $fetchResult->getQueueItem()->getId();

        try {
            $part = $this->analyzer->analyze($fetchResult->getContent());
        } catch (\LogicException $e) {
            Console::trace(sprintf('Nissan part was not identified by analyzer (queue#%d): %s', $queueItemId, $e->getMessage()));
            $part = null;
        }

        if (! $part) {
            return;
        }

        if ($this->partRepository->findByNumber($part->getNumber())) {
            Console::trace('Nissan part already exists: ' . $part->getNumber());
        }

        $part->setImportId($queueItemId);
        if (! $this->partRepository->save($part)) {
            Console::trace('Could not import nissan part: queue#' . $queueItemId);
            return;
        }

        Console::trace('Nissan part imported: ' . $part->getNumber() ? : 'no number');
    }
}
