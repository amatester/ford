<?php

namespace App\Queue\Repository;

use App\Queue\Queue;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * Queue repository implementation.
 */
class QueueRepository extends ServiceEntityRepository implements IQueueRepository
{
    /**
     * @param ManagerRegistry $registry
     */
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Queue::class);
    }
    
    /**
     * @inheritdoc
     */
    public function createBatch($queueItems)
    {
        foreach ($queueItems as $queueItem) {
            $this->persist($queueItem);
        }

        $this->getEntityManager()->flush();
    }

    /**
     * @inheritdoc
     */
    public function getItemsForFetching($limit, $make)
    {
        if (empty($limit) || empty($make)) {
            return [];
        }

        /* @var Queue[] $items */
        $items = $this->createQueryBuilder('queue')
            ->where('queue.status = :new AND queue.make = :make')
            ->addSelect('CASE WHEN queue.type = :part THEN 0 ELSE 1 END AS HIDDEN sort')
            ->orderBy('sort', 'ASC')
            ->addOrderBy('queue.id', 'ASC')
            ->setMaxResults($limit)
            ->setParameter('new', Queue::STATUS_NEW)
            ->setParameter('part', Queue::TYPE_PART)
            ->setParameter('make', (string)$make)
            ->getQuery()
            ->getResult();
        if (! $items) {
            return [];
        }

        foreach ($items as $item) {
            $item->setStatus(Queue::STATUS_IN_PROGRESS);
        }

        $this->updateBatch($items);

        return $items;
    }

    /**
     * @inheritdoc
     */
    public function hasUrl($url)
    {
        $row = $this->createQueryBuilder('queue')
            ->select('queue.id')
            ->where('queue.url = :url')
            ->setParameter('url', $url)
            ->getQuery()
            ->getOneOrNullResult();

        if (! $row) {
            return false;
        }

        return true;
    }

    /**
     * @inheritdoc
     */
    public function setStatus($queueItems, $status)
    {
        if (empty($queueItems)) {
            return;
        }

        foreach ($queueItems as $queueItem) {
            $queueItem->setStatus($status);
        }

        $this->updateBatch($queueItems);
    }

    /**
     * @param Queue[] $queueItems
     */
    private function updateBatch($queueItems)
    {
        if (empty($queueItems)) {
            return;
        }

        foreach ($queueItems as $queueItem) {
            $this->persist($queueItem);
        }

        $this->getEntityManager()->flush();
    }

    /**
     * @param Queue $queueItem
     */
    private function persist(Queue $queueItem)
    {
        $this->getEntityManager()->persist($queueItem);
    }
}
