<?php

namespace App\Toyota\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Toyota\Repository\ToyotaPartRepository")
 * @ORM\Table(name="toyota_part")
 */
class ToyotaPart
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     * @var int
     */
    private $id;
    /**
     * @ORM\Column(type="integer")
     * @var int
     */
    private $importId;
    /**
     * @ORM\Column(type="string")
     * @var string
     */
    private $number;
    /**
     * @ORM\Column(type="string")
     * @var string
     */
    private $msrpPrice;
    /**
     * @ORM\Column(type="string")
     * @var string
     */
    private $title;
    /**
     * @ORM\Column(type="string")
     * @var string
     */
    private $unit;
    /**
     * @ORM\Column(type="string")
     * @var string
     */
    private $description;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return ToyotaPart
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return int
     */
    public function getImportId()
    {
        return $this->importId;
    }

    /**
     * @param int $importId
     * @return ToyotaPart
     */
    public function setImportId($importId)
    {
        $this->importId = $importId;
        return $this;
    }

    /**
     * @return string
     */
    public function getNumber()
    {
        return $this->number;
    }

    /**
     * @param string $number
     * @return ToyotaPart
     */
    public function setNumber($number)
    {
        $this->number = $number;
        return $this;
    }

    /**
     * @return string
     */
    public function getMsrpPrice()
    {
        return $this->msrpPrice;
    }

    /**
     * @param string $msrpPrice
     * @return ToyotaPart
     */
    public function setMsrpPrice($msrpPrice)
    {
        $this->msrpPrice = $msrpPrice;
        return $this;
    }

    /**
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param string $title
     * @return ToyotaPart
     */
    public function setTitle($title)
    {
        $this->title = $title;
        return $this;
    }

    /**
     * @return string
     */
    public function getUnit()
    {
        return $this->unit;
    }

    /**
     * @param string $unit
     * @return ToyotaPart
     */
    public function setUnit($unit)
    {
        $this->unit = $unit;
        return $this;
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param string $description
     * @return ToyotaPart
     */
    public function setDescription($description)
    {
        $this->description = $description;
        return $this;
    }
}
