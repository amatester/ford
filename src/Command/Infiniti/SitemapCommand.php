<?php

namespace App\Command\Infiniti;

use App\Helper\Console;
use App\Queue\Queue;
use App\Queue\Repository\IQueueRepository;
use GuzzleHttp\Client;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\HttpFoundation\Response;

/**
 * Command for handling Infiniti sitemaps.
 */
class SitemapCommand extends ContainerAwareCommand
{
    /**
     * @var Client
     */
    private $httpClient;
    /**
     * @var IQueueRepository
     */
    private $queueRepository;

    /**
     * @inheritdoc
     */
    protected function configure()
    {
        $this->setName('infiniti:sitemap');
    }

    /**
     * @param IQueueRepository $queueRepository
     */
    public function __construct(IQueueRepository $queueRepository) {
        $this->queueRepository = $queueRepository;
        $this->httpClient = new Client();

        parent::__construct();
    }

    /**
     * @inheritdoc
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        Console::setOutput($output);

        try {
            $base = $this->fetchBase('https://www.discountinfinitiparts.com/sitemap.xml');
        } catch (\LogicException $e) {
            Console::trace($e->getMessage());
            return;
        }

        $xml = simplexml_load_string($base);
        foreach ($xml as $item) {
            if (empty($item->loc) || ! strpos($item->loc, 'spp_infiniti')) {
                continue;
            }

            $parts = simplexml_load_string($this->fetchPartsSitemap($item->loc));
            if (empty($parts)) {
                continue;
            }

            Console::trace('Parsing sitemap: ' . $item->loc);

            $queueItems = [];
            foreach ($parts as $part) {
                if (empty($part->loc)) {
                    continue;
                }

                $partUrl = (string)$part->loc;
                if ($this->queueRepository->hasUrl($partUrl)) {
                    continue;
                }

                $queueItems[$partUrl] = (new Queue())
                    ->setUrl($partUrl)
                    ->setMake(Queue::MAKE_INFINITI)
                    ->setType(Queue::TYPE_PART)
                    ->setStatus(Queue::STATUS_NEW)
                    ->setDateCreated(date('Y-m-d H:i:s'));
            }

            $this->queueRepository->createBatch($queueItems);
            Console::trace('Added queue items: ' . count($queueItems));
        }
    }

    /**
     * @param string $url
     * @return string
     *
     * @throws \LogicException
     */
    private function fetchBase($url)
    {
        $response = $this->httpClient->get($url);
        if ($response->getStatusCode() !== Response::HTTP_OK) {
            throw new \LogicException('Not 200 fetching base sitemap');
        }

        return $response->getBody();
    }

    /**
     * @param string $url
     * @return string
     */
    private function fetchPartsSitemap($url)
    {
        return implode(PHP_EOL, gzfile($url));
    }
}
