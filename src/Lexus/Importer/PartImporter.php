<?php

namespace App\Lexus\Importer;

use App\Fetcher\FetchResult;
use App\Helper\Console;
use App\Importer\ImporterInterface;
use App\Lexus\Analyzer\PartPageAnalyzer;
use App\Lexus\Repository\IPartRepository;

/**
 * Lexus part importer.
 */
class PartImporter implements ImporterInterface
{
    /**
     * @var PartPageAnalyzer
     */
    private $analyzer;
    /**
     * @var IPartRepository
     */
    private $partRepository;

    /**
     * @param PartPageAnalyzer $analyzer
     * @param IPartRepository $partRepository
     */
    public function __construct(PartPageAnalyzer $analyzer, IPartRepository $partRepository)
    {
        $this->analyzer = $analyzer;
        $this->partRepository = $partRepository;
    }

    /**
     * @param FetchResult $fetchResult
     */
    public function import(FetchResult $fetchResult)
    {
        $queueItemId = $fetchResult->getQueueItem()->getId();

        try {
            $part = $this->analyzer->analyze($fetchResult->getContent());
        } catch (\LogicException $e) {
            Console::trace(sprintf('Lexus part was not identified by analyzer (queue#%d): %s', $queueItemId, $e->getMessage()));
            $part = null;
        }

        if (! $part) {
            return;
        }

        $part->setImportId($queueItemId);

        $savedPart = $this->partRepository->findByNumber($part->getNumber());
        if ($savedPart) {
            $savedPart->setMsrpPrice($part->getMsrpPrice());
            $part = $savedPart;
            Console::trace('Lexus part already exists: ' . $part->getNumber());
        }

        if (! $this->partRepository->save($part)) {
            Console::trace('Could not import lexus part: queue#' . $queueItemId);
            return;
        }

        Console::trace('Lexus part imported: ' . $part->getNumber() ? : 'no number');
    }
}
