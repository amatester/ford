<?php

namespace App\Queue\Service;

use App\Fetcher\IFetcher;
use App\Helper\Console;
use App\Importer\ImporterInterface;
use App\Queue\Queue;
use App\Queue\Repository\IQueueRepository;

/**
 * Queue handler: looping through the queue, fetching data and importing it.
 *
 * @property IQueueRepository $queueRepository
 * @property ImporterInterface $importer
 * @property IFetcher $fetcher
 */
trait QueueHandler
{
    /**
     * Handles queue.
     *
     * @param string $make
     * @param int $concurrentRequestsPerLoop
     */
    public function handle($concurrentRequestsPerLoop, $make)
    {
        while (true) {
            $queueItems = $this->queueRepository->getItemsForFetching($concurrentRequestsPerLoop, $make);
            if (! $queueItems) {
                Console::trace('No queue items to fetch');
                return;
            }

            Console::trace('=== NEXT ===');
            Console::trace(sprintf('Got next %d queue items', count($queueItems)));

            $fetchResults = $this->fetcher->fetch($queueItems, $make);
            if (! $fetchResults) {
                $this->queueRepository->setStatus($queueItems, Queue::STATUS_NEW);
                Console::trace('Nothing is fetched');

                Console::sleep(rand(10, 15));
                continue;
            }

            $successQueueItems = [];
            $failQueueItems = [];
            foreach ($fetchResults as $result) {
                $queueItem = $result->getQueueItem();

                if (! $result->isSuccess()) {
                    $failQueueItems[] = $queueItem;
                    Console::trace(sprintf('queue item #%d [%s] fetch FAILED', $queueItem->getId(), $queueItem->getType()));
                    continue;
                }

                $successQueueItems[] = $queueItem;
                Console::trace(sprintf('queue item #%d [%s] fetch SUCCESS', $queueItem->getId(), $queueItem->getType()));
                $this->importer->import($result);
            }

            $this->queueRepository->setStatus($successQueueItems, Queue::STATUS_FETCHED);
            $this->queueRepository->setStatus($failQueueItems, Queue::STATUS_NEW);

            Console::sleep(rand(3, 10));
        }
    }
}
