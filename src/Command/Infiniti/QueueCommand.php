<?php

namespace App\Command\Infiniti;

use App\Fetcher\IFetcher;
use App\Helper\Console;
use App\Infiniti\Importer\Importer;
use App\Queue\Queue;
use App\Queue\Repository\IQueueRepository;
use App\Queue\Repository\QueueRepository;
use App\Queue\Service\QueueHandler;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Command for handling Infiniti queue.
 */
class QueueCommand extends ContainerAwareCommand
{
    use QueueHandler;

    /**
     * @var int
     */
    private $limit;
    /**
     * @var IQueueRepository
     */
    private $queueRepository;
    /**
     * @var IFetcher
     */
    private $fetcher;
    /**
     * @var Importer
     */
    private $importer;

    /**
     * @inheritdoc
     */
    protected function configure()
    {
        $this->setName('infiniti:queue');
    }

    /**
     * @inheritdoc
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->setUp($output);
        $this->handle($this->limit, Queue::MAKE_INFINITI);
    }

    /**
     * Setting up dependencies.
     *
     * @param OutputInterface $output
     */
    private function setUp(OutputInterface $output)
    {
        Console::setOutput($output);

        $container = $this->getContainer();
        $this->limit = $container->getParameter('fetch.limit');
        $this->fetcher = $container->get(IFetcher::class);
        $this->queueRepository = $container->get(QueueRepository::class);
        $this->importer = $container->get(Importer::class);
    }
}
