<?php declare(strict_types = 1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;
use Doctrine\DBAL\Types\Type;

/**
 * Making unit field as Text in part tables.
 */
class Version20180507013002 extends AbstractMigration
{
    public function up(Schema $schema)
    {
        $schema->getTable('nissan_part')->changeColumn('unit', ['type' => Type::getType(Type::TEXT)]);
        $schema->getTable('lexus_part')->changeColumn('unit', ['type' => Type::getType(Type::TEXT)]);
        $schema->getTable('toyota_part')->changeColumn('unit', ['type' => Type::getType(Type::TEXT)]);
        $schema->getTable('infiniti_part')->changeColumn('unit', ['type' => Type::getType(Type::TEXT)]);
    }

    public function down(Schema $schema)
    {

    }
}
