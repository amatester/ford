<?php

namespace App\Toyota\Importer;

use App\Fetcher\FetchResult;
use App\Helper\Console;
use App\Importer\ImporterInterface;
use App\Queue\Queue;
use App\Toyota\Analyzer\Analyzer;

/**
 * ImporterInterface.
 */
class Importer implements ImporterInterface
{
    /**
     * @var Analyzer
     */
    private $analyzer;
    /**
     * @var PartImporter
     */
    private $partImporter;

    /**
     * @param Analyzer $analyzer
     * @param PartImporter $partImporter
     */
    public function __construct(Analyzer $analyzer, PartImporter $partImporter)
    {
        $this->analyzer = $analyzer;
        $this->partImporter = $partImporter;
    }

    /**
     * @param FetchResult $fetchResult
     */
    public function import(FetchResult $fetchResult)
    {
        $queueItem = $fetchResult->getQueueItem();

        switch ($queueItem->getType()) {
            case Queue::TYPE_PART:
                $part = $this->analyzer->analyzePagePart($fetchResult->getContent());
                if ($part) {
                    $part->setImportId($queueItem->getId());
                    $this->partImporter->import($part);
                } else {
                    Console::trace('Toyota part was not identified by analyzer: queue#' . $queueItem->getId());
                }

                break;
        }
    }
}
