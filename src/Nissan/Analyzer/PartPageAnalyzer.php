<?php

namespace App\Nissan\Analyzer;

use App\Helper\Parser;
use App\Nissan\Entity\Part;
use voku\helper\HtmlDomParser;

/**
 * Analyzer for lexus part page.
 */
class PartPageAnalyzer
{
    /**
     * Analyzing part page.
     *
     * @param string $content
     * @return Part
     */
    public function analyze($content)
    {
        $html = HtmlDomParser::str_get_html($content);

        $cartAddBtnEl = $html->find('.cart > a.add', 0);
        if (! $cartAddBtnEl) {
            throw new \LogicException('No element for parse part number and title');
        }

        $dataNbr = 'data-nbr';
        $partNumber = $cartAddBtnEl->$dataNbr;
        if (! $partNumber) {
            throw new \LogicException('No part number');
        }

        $dataDescr = 'data-desc';
        $title = $cartAddBtnEl->$dataDescr;
        if (! $title) {
            throw new \LogicException('No part title');
        }

        $part = (new Part())
            ->setNumber($partNumber)
            ->setTitle($title);

        $priceEl = $html->find('.retail > span', 1);
        if ($priceEl) {
            $part->setMsrpPrice(Parser::preparePrice($priceEl->plaintext));
        }

        $descriptionEl = $html->find('td.desc', 0);
        if ($descriptionEl
            && ! empty($descriptionEl->plaintext)
            && $descriptionEl->plaintext != $part->getTitle()
        ) {
            $part->setDescription($descriptionEl->plaintext);
        }

        $dataReplaced = 'data-replace';
        if (! empty($cartAddBtnEl->$dataReplaced)) {
            $part->setDescription(trim(sprintf(
                "%s\nReplaced by: %s",
                $part->getDescription(),
                $cartAddBtnEl->$dataReplaced
            )));
        }

        return $part;
    }
}
