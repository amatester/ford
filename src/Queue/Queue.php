<?php

namespace App\Queue;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Queue\Repository\QueueRepository")
 * @ORM\Table(name="queue")
 */
class Queue
{
    const TYPE_YEAR = 'YEAR';
    const TYPE_MAKE = 'MAKE';
    const TYPE_MODEL = 'MODEL';
    const TYPE_MODIFICATION = 'MODIFICATION';
    const TYPE_CATEGORY = 'CATEGORY';
    const TYPE_PART = 'PART';

    const STATUS_NEW = 0;
    const STATUS_IN_PROGRESS = 1;
    const STATUS_FETCHED = 2;

    const MAKE_TOYOTA = 'TOYOTA';
    const MAKE_LEXUS = 'LEXUS';
    const MAKE_NISSAN = 'NISSAN';
    const MAKE_INFINITI = 'INFINITI';

    /**
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     * @var ?int
     */
    private $id;
    /**
     * @ORM\Column(type="string", unique=true)
     * @var ?string
     */
    private $url;
    /**
     * @ORM\Column(type="string")
     * @var ?string
     */
    private $type;
    /**
     * @ORM\Column(type="integer")
     * @var ?int
     */
    private $parentId;
    /**
     * @ORM\Column(type="string")
     * @var ?string
     */
    private $dateCreated;
    /**
     * @ORM\Column(type="integer")
     * @var ?int
     */
    private $status;
    /**
     * @ORM\Column(type="string")
     * @var ?string
     */
    private $make;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return Queue
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return string
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * @param string $url
     * @return Queue
     */
    public function setUrl($url)
    {
        $this->url = $url;
        return $this;
    }

    /**
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param string $type
     * @return Queue
     */
    public function setType($type)
    {
        $this->type = $type;
        return $this;
    }

    /**
     * @return int
     */
    public function getParentId()
    {
        return $this->parentId;
    }

    /**
     * @param int $parentId
     * @return Queue
     */
    public function setParentId($parentId)
    {
        $this->parentId = $parentId;
        return $this;
    }

    /**
     * @return string
     */
    public function getDateCreated()
    {
        return $this->dateCreated;
    }

    /**
     * @param string $dateCreated
     * @return Queue
     */
    public function setDateCreated($dateCreated)
    {
        $this->dateCreated = $dateCreated;
        return $this;
    }

    /**
     * @return int
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param int $status
     * @return Queue
     */
    public function setStatus($status)
    {
        $this->status = $status;
        return $this;
    }

    /**
     * @return string
     */
    public function getMake()
    {
        return $this->make;
    }

    /**
     * @param string $make
     * @return Queue
     */
    public function setMake($make)
    {
        $this->make = $make;
        return $this;
    }
}
