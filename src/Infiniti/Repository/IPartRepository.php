<?php

namespace App\Infiniti\Repository;

use App\Infiniti\Entity\Part;

/**
 * Repository for Infiniti parts.
 */
interface IPartRepository
{
    /**
     * @param Part $part
     * @return Part
     */
    public function save(Part $part);

    /**
     * @param string $number
     * @return Part
     */
    public function findByNumber($number);
}
