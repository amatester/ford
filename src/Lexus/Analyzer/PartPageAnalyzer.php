<?php

namespace App\Lexus\Analyzer;

use App\Helper\Parser;
use App\Lexus\Entity\Part;
use voku\helper\HtmlDomParser;

/**
 * Analyzer for lexus part page.
 */
class PartPageAnalyzer
{
    /**
     * Analyzing part page.
     *
     * @param string $content
     * @return Part
     */
    public function analyze($content)
    {
        $html = HtmlDomParser::str_get_html($content);

        $partNumber = $html->find('.part_number', 0)->find('span', 0)->plaintext;
        if (! $partNumber) {
            throw new \LogicException('No part number');
        }

        $title = $html->find('meta[name="og:title"]', 0)->content;
        if (! $title) {
            throw new \LogicException('No part title');
        }

        $part = (new Part())
            ->setNumber($partNumber)
            ->setTitle($title);

        $priceEl = $html->find('#product_price2', 0);
        if ($priceEl) {
            $part->setMsrpPrice(Parser::preparePrice($priceEl->plaintext));
        }

        $otherNamesEl = $html->find('.also_known_as > span', 0);
        if ($otherNamesEl) {
            $part->setUnit($otherNamesEl->plaintext);
        }

        $descriptionEl = $html->find('.field_list > .product-description > .description_body', 0);
        if ($descriptionEl) {
            $part->setDescription($descriptionEl->plaintext);
        }

        $supersededEl = $html->find('.product-superseded-list > span', 0);
        if ($supersededEl && ! empty($supersededEl->plaintext)) {
            $part->setDescription(trim(sprintf(
                "%s\nReplaces: %s",
                $part->getDescription(),
                $supersededEl->plaintext
            )));
        }

        return $part;
    }
}
