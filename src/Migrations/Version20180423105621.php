<?php

namespace DoctrineMigrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;
use Doctrine\DBAL\Types\Type;

/**
 * Creating `queue` table.
 */
class Version20180423105621 extends AbstractMigration
{
    /**
     * @var string
     */
    private $queue = 'queue';

    /**
     * @inheritdoc
     */
    public function up(Schema $schema)
    {
        $table = $schema->createTable($this->queue);
        $table->addColumn('id', Type::INTEGER, [
            'autoincrement' => true
        ]);
        $table->addColumn('url', Type::STRING);
        $table->addColumn('type', Type::STRING);
        $table->addColumn('parent_id', Type::INTEGER, [
            'notnull' => false
        ]);
        $table->addColumn('date_created', Type::STRING);
        $table->addColumn('status', Type::INTEGER, [
            'default' => 0
        ]);

        $table->addUniqueIndex(['url']);
        $table->setPrimaryKey(['id']);
    }

    /**
     * @inheritdoc
     */
    public function down(Schema $schema)
    {
        $schema->dropTable($this->queue);
    }
}
