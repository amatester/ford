<?php

namespace App\Proxy;

/**
 * Client for Proxy API.
 */
interface IProxyClient
{
    /**
     * Getting proxies for requests.
     *
     * @param int $limit
     * @return Proxy[]
     */
    public function getProxies($limit);

    /**
     * Sending info about bad proxy.
     *
     * @param Proxy $proxy
     * @return mixed
     */
    public function sendBadProxy(Proxy $proxy);
}
