<?php

namespace App\Lexus\Repository;

use App\Lexus\Entity\Part;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * Lexus part repository.
 */
class PartRepository extends ServiceEntityRepository implements IPartRepository
{
    /**
     * @param ManagerRegistry $registry
     */
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Part::class);
    }

    /**
     * @inheritdoc
     */
    public function save(Part $part)
    {
        $this->getEntityManager()->persist($part);
        $this->getEntityManager()->flush();

        return $part;
    }

    /**
     * @inheritdoc
     */
    public function findByNumber($number)
    {
        $parts = $this->createQueryBuilder('part')
            ->where('part.number = :number')
            ->setParameter('number', (string)$number)
            ->getQuery()
            ->getResult();
        if (! $parts) {
            return null;
        }

        return reset($parts);
    }
}
