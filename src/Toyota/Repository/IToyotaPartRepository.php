<?php

namespace App\Toyota\Repository;

use App\Toyota\Entity\ToyotaPart;

/**
 * Toyota parts repository.
 */
interface IToyotaPartRepository
{
    /**
     * @param ToyotaPart $part
     * @return ToyotaPart
     */
    public function create(ToyotaPart $part);

    /**
     * @param string $number
     * @return ToyotaPart
     */
    public function findByNumber($number);
}
