<?php

namespace App\Proxy;

/**
 * Stub for IProxyClient.
 */
class ProxyClientStub implements IProxyClient
{
    /**
     * @inheritdoc
     */
    public function getProxies($limit)
    {
        return [];
    }

    /**
     * @inheritdoc
     */
    public function sendBadProxy(Proxy $proxy)
    {
        return;
    }
}
