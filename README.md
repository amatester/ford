Toyota
------
- `bin/console toyota:sitemap` Стягивает sitemap'ы и добавляет в очередь новые записи.
- `bin/console toyota:queue` Обходит очередь и стягивает данные с донора.


Lexus
------
- `bin/console lexus:sitemap` Стягивает sitemap'ы и добавляет в очередь новые записи.
- `bin/console lexus:queue` Обходит очередь и стягивает данные с донора.