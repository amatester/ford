<?php

namespace App\Fetcher;

use App\Queue\Queue;

/**
 * Result of fetching data from donor.
 */
class FetchResult
{
    /**
     * @const string
     */
    const RESULT_SUCCESS = 'SUCCESS';
    /**
     * @const string
     */
    const RESULT_FAIL = 'FAIL';

    /**
     * @var Queue
     */
    private $queueItem;
    /**
     * @var string
     */
    private $content;
    /**
     * @var string
     */
    private $result;

    /**
     * @return Queue
     */
    public function getQueueItem()
    {
        return $this->queueItem;
    }

    /**
     * @param Queue $queueItem
     * @return FetchResult
     */
    public function setQueueItem(Queue $queueItem = null)
    {
        $this->queueItem = $queueItem;
        return $this;
    }

    /**
     * @return string
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * @param string $content
     * @return FetchResult
     */
    public function setContent($content)
    {
        $this->content = $content;
        return $this;
    }

    /**
     * @param string $result
     * @return FetchResult
     */
    public function setResult($result)
    {
        $this->result = $result;
        return $this;
    }

    /**
     * @return bool
     */
    public function isSuccess()
    {
        return $this->result == self::RESULT_SUCCESS;
    }
}
