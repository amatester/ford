<?php

namespace App\Lexus\Importer;

use App\Fetcher\FetchResult;
use App\Importer\ImporterInterface;
use App\Queue\Queue;

/**
 * Lexus importer.
 */
class Importer implements ImporterInterface
{
    /**
     * @var PartImporter
     */
    private $partImporter;

    /**
     * @param PartImporter $partImporter
     */
    public function __construct(PartImporter $partImporter)
    {
        $this->partImporter = $partImporter;
    }

    /**
     * @inheritdoc
     */
    public function import(FetchResult $fetchResult)
    {
        switch ($fetchResult->getQueueItem()->getType()) {
            case Queue::TYPE_PART:
                $this->partImporter->import($fetchResult);
                break;
        }
    }
}
