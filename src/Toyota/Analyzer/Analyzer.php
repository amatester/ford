<?php

namespace App\Toyota\Analyzer;

use App\Helper\Parser;
use App\Toyota\Entity\ToyotaPart;
use voku\helper\HtmlDomParser;

/**
 * Toyota pages analyzer.
 */
class Analyzer
{
    /**
     * @param string $content
     * @return ToyotaPart
     */
    public function analyzePagePart($content)
    {
        $html = HtmlDomParser::str_get_html($content);

        $partNumber = $html->find('.part_number', 0)->find('span.list_value', 0)->plaintext;
        if (! $partNumber) {
            throw new \LogicException('No part number');
        }

        $title = $html->find('.part-notes', 0)->find('span.list_value', 0)->plaintext;
        if (! $title) {
            throw new \LogicException('No part title');
        }

        $part = (new ToyotaPart())
            ->setNumber($partNumber)
            ->setTitle($title);

        $priceEl = $html->find('#product_price2', 0);
        if ($priceEl) {
            $part->setMsrpPrice(Parser::preparePrice($priceEl->plaintext));
        }

        $otherNamesEl = $html->find('.also_known_as > span', 0);
        if ($otherNamesEl) {
            $part->setUnit($otherNamesEl->plaintext);
        }

        $descriptionEl = $html->find('.field_list > .product-description > .description_body', 0);
        if ($descriptionEl) {
            $part->setDescription($descriptionEl->plaintext);
        }

        return $part;
    }
}
