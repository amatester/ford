<?php

namespace App\Proxy;

/**
 * Proxy object.
 */
class Proxy
{
    /**
     * @const int
     */
    const DEFAULT_TIMEOUT = 60;
    /**
     * @const int
     */
    const MIN_TIMEOUT = 20;
    /**
     * @var string
     */
    private $host;
    /**
     * @var string
     */
    private $port;
    /**
     * @var int
     */
    private $timeout;
    /**
     * @var string
     */
    private $protocol;

    /**
     * @return string
     */
    public function getHost()
    {
        return $this->host;
    }

    /**
     * @param string $host
     * @return Proxy
     */
    public function setHost($host)
    {
        $this->host = $host;
        return $this;
    }

    /**
     * @return string
     */
    public function getPort()
    {
        return $this->port;
    }

    /**
     * @param string $port
     * @return Proxy
     */
    public function setPort($port)
    {
        $this->port = $port;
        return $this;
    }

    /**
     * @return int
     */
    public function getTimeout()
    {
        if (! $this->timeout) {
            return self::DEFAULT_TIMEOUT;
        }

        if ($this->timeout < self::MIN_TIMEOUT) {
            return self::MIN_TIMEOUT;
        }

        return $this->timeout;
    }

    /**
     * @param int $timeout
     * @return Proxy
     */
    public function setTimeout($timeout)
    {
        $this->timeout = $timeout;
        return $this;
    }

    /**
     * @return string
     */
    public function getProtocol()
    {
        return $this->protocol;
    }

    /**
     * @param string $protocol
     * @return Proxy
     */
    public function setProtocol($protocol)
    {
        $this->protocol = $protocol;
        return $this;
    }

    /**
     * @return string
     */
    public function getRequestHost()
    {
        $base = sprintf('%s:%s', $this->getHost(), $this->getPort());
        if (! $this->getProtocol()) {
            return $base;
        }

        return sprintf('%s://%s', mb_strtolower($this->getProtocol()), $base);
    }

    /**
     * @return string
     */
    public function getProxyString()
    {
        return sprintf('%s (%d)', $this->getRequestHost(), $this->getTimeout());
    }
}
