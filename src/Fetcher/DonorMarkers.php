<?php

namespace App\Fetcher;

use App\Queue\Queue;

/**
 * Markers that helps to ensure that donor page was download properly.
 */
class DonorMarkers
{
    /**
     * @var array
     */
    private static $markers = [
        Queue::MAKE_LEXUS => 'toyolexparts</title>',
        Queue::MAKE_TOYOTA => 'Toyota Parts</title>',
        Queue::MAKE_NISSAN => '<a class="logo " href="https://www.nissanpartsdeal.com">',
        Queue::MAKE_INFINITI => 'Discount Infiniti Parts</title>',
    ];

    /**
     * @param string $donor
     * @return string
     */
    public static function getMarker($donor)
    {
        if (empty(self::$markers[$donor])) {
            throw new \LogicException('No marker for donor: ' . $donor);
        }

        return self::$markers[$donor];
    }
}
