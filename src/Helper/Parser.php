<?php

namespace App\Helper;

/**
 * Helper methods for parsed data.
 */
class Parser
{
    /**
     * @param string $price
     * @return string
     */
    public static function preparePrice($price)
    {
        return preg_replace('/[^0-9.,]/', '', $price);
    }
}
