<?php

namespace App\Toyota\Importer;

use App\Helper\Console;
use App\Toyota\Entity\ToyotaPart;
use App\Toyota\Repository\IToyotaPartRepository;

/**
 * ImporterInterface for Toyota parts.
 */
class PartImporter
{
    /**
     * @var IToyotaPartRepository
     */
    private $partRepository;

    /**
     * @param IToyotaPartRepository $partRepository
     */
    public function __construct(IToyotaPartRepository $partRepository)
    {
        $this->partRepository = $partRepository;
    }

    /**
     * @param ToyotaPart $part
     */
    public function import(ToyotaPart $part)
    {
        if ($this->partRepository->findByNumber($part->getNumber())) {
            Console::trace('Toyota part already exists: ' . $part->getNumber());

            return;
        }

        if (! $this->partRepository->create($part)) {
            Console::trace('Could not import toyota part: ' . $part->getNumber());
            return;
        }

        Console::trace('Toyota part imported: ' . $part->getNumber());
    }
}
