<?php

namespace App\Proxy;

use App\Helper\Console;
use GuzzleHttp\Client;
use GuzzleHttp\RequestOptions;

/**
 * IProxyClient implementation.
 */
class ProxyClient implements IProxyClient
{
    /**
     * @const string
     */
    const METHOD_LIST = 'list';
    /**
     * @const string
     */
    const METHOD_MARK_BAD = 'bad';
    /**
     * @var string
     */
    private $apiHost;
    /**
     * @var string
     */
    private $httpClient;

    /**
     * @param string $host
     */
    public function __construct(string $host = '')
    {
        $this->apiHost = $host;
        $this->httpClient = new Client();
    }

    /**
     * @inheritdoc
     */
    public function getProxies($limit)
    {
        $proxies = $this->getDecodedJson($this->buildUrl(self::METHOD_LIST, ['count' => $limit]));
        if (! $proxies) {
            return [];
        }

        $result = [];
        foreach ($proxies as $proxy) {
            $result[] = (new Proxy())
                ->setHost($proxy['host'])
                ->setPort($proxy['port'])
                ->setProtocol($proxy['protocol'])
                ->setTimeout($proxy['timeout']);
        }

        return $result;
    }

    /**
     * @inheritdoc
     */
    public function sendBadProxy(Proxy $proxy)
    {
        try {
            $this->httpClient->post($this->buildUrl('bad'), [
                RequestOptions::JSON => [
                    'host' => $proxy->getHost(),
                    'port' => $proxy->getPort()
                ]
            ]);
        } catch (\Exception $e) {
            Console::trace('Could not send bad proxy: ' . $e->getMessage());
        }
    }

    /**
     * @param string $url
     * @return array
     */
    private function getDecodedJson($url)
    {
        $result = $this->httpClient->get($url);
        $json = $result->getBody();

        return json_decode($json, true);
    }

    /**
     * @param string $method
     * @param array $params
     * @return string
     */
    private function buildUrl($method, $params = [])
    {
        $url = sprintf('%s/%s', $this->apiHost, $method);
        if (! $params) {
            return $url;
        }

        return sprintf('%s/?%s', $url, http_build_query($params));
    }
}
