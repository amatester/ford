<?php

namespace App\Queue\Repository;

use App\Queue\Queue;

/**
 * Queue repository.
 */
interface IQueueRepository
{
    /**
     * @param Queue[] $queueItems
     */
    public function createBatch($queueItems);

    /**
     * @param string $url
     * @return bool
     */
    public function hasUrl($url);

    /**
     * @param int $limit
     * @param string $make
     * @return Queue[]
     */
    public function getItemsForFetching($limit, $make);

    /**
     * @param Queue[] $queueItems
     * @param string $status
     */
    public function setStatus($queueItems, $status);
}
