<?php

namespace App\Fetcher;

use App\Queue\Queue;

/**
 * Fetching service.
 */
interface IFetcher
{
    /**
     * @param Queue[] $queue
     * @param string $donorName
     * @return FetchResult[]
     */
    public function fetch($queue, $donorName);
}
