<?php

namespace DoctrineMigrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;
use Doctrine\DBAL\Types\Type;

/**
 * Adding `make` field to `queue` table.
 */
class Version20180426083726 extends AbstractMigration
{
    /**
     * @var string
     */
    private $queue = 'queue';
    /**
     * @var string
     */
    private $make = 'make';

    /**
     * @inheritdoc
     */
    public function up(Schema $schema)
    {
        $table = $schema->getTable($this->queue);
        $table->addColumn($this->make, Type::STRING);
        $table->addIndex([$this->make], 'queue_make_idx');
    }

    /**
     * @inheritdoc
     */
    public function down(Schema $schema)
    {
        $schema
            ->getTable($this->queue)
            ->dropColumn($this->make);
    }
}
