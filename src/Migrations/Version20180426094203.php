<?php

namespace DoctrineMigrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;
use Doctrine\DBAL\Types\Type;

/**
 * Creating `lexus_part` table.
 */
class Version20180426094203 extends AbstractMigration
{
    /**
     * @var string
     */
    private $lexusParts = 'lexus_part';

    /**
     * @inheritdoc
     */
    public function up(Schema $schema)
    {
        $table = $schema->createTable($this->lexusParts);
        $table->addColumn('id', Type::INTEGER, [
            'autoincrement' => true
        ]);
        $table->addColumn('import_id', Type::INTEGER);
        $table->addColumn('number', Type::STRING);
        $table->addColumn('msrp_price', Type::STRING, [
            'notnull' => false
        ]);
        $table->addColumn('title', Type::STRING);
        $table->addColumn('unit', Type::STRING, [
            'notnull' => false
        ]);
        $table->addColumn('description', Type::TEXT, [
            'notnull' => false
        ]);

        $table->setPrimaryKey(['id']);
        $table->addUniqueIndex(['number']);
    }

    /**
     * @inheritdoc
     */
    public function down(Schema $schema)
    {
        $schema->dropTable($this->lexusParts);
    }
}
